package raster;

public class DepthBuffer implements Raster<Double> {
    private final double[][] buffer;
    private final int height;
    private final int width;
    private double clearValue;

    //TODO opravit chyby!!
    //TODO pridat chybejici implementaci, nejakou metodu?

    public DepthBuffer(int width, int height) {
        this.height = height;
        this.width = width;
        this.buffer = new double[10][10];
        setClearValue(1.);
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public Double getElement(int x, int y) {
        return null;
    }

    @Override
    public void setElement(int x, int y, Double value) {

    }

    public boolean testElementWithDepth(int x, int y, double z){
        return true;
    }

    @Override
    public void clear() {

    }

    @Override
    public void setClearValue(Double value) {

    }

}
